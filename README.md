# Internet, Web e Cloud - sede Mantova

## Esercitazioni Python 

### Convenzioni

Questa sezione è ancora in fae di sviluppo, per ora elenco semplicemente una lista di acronimi che utilizzo spesso:
- WIP: Work In Progress, indica un qualcosa che è ancora in fase di completamente/sviluppo
- TODO: indica qualcosa che deve ancora essere realizzato e caricato

### Organizzazione repo

Nei vari branch di questa repo sono disponibili informazioni ed esercizi relativi ai diversi livelli di python richiesti per esercizi o laboratori durante il corso.
Attualmente i branch disponibili sono:
- main: contiene solo istruzioni di base al momento
- python-base: slide ed esercizi base su python
- python-intermediate: slide ed esercizi su classi, packages e magic methods

### branch: python-base

Nella repository sono presenti una serie di slide e Jupyter Notebook. Di seguito un elenco dettagliato di cosa è presente:

- 0.0.installazioni_python_virtualenv_pycharm.pdf: slide contenenti le istruzioni iniziali per installare i componenti base (e opzionali) necessari per seguire le lezioni
- 0.0.tool_utili.pdf: slide contenenti una serie di tool che rispondono alla domanda "ma cosa sta utilizzando per far quello?"
- 0.1.python-base-1.pdf: slide base su python, sostanzialmente una super carrellata di concetti di ripasso su come si scrive in python
- 0.1.python.pep20_by_example.pdf: un bellissimo documento che spiega i principi del PEP20 punto per punto, gentilmente offerto da Mauro Riva nel suo blog: https://lemariva.com/storage/app/media/uploaded-files/pep20_by_example.pdf
- 0.1.python-getting_started.ipynb: notebook contenente istruzioni per l'utilizzo degli jupyter notebook ed alcuni comandi di ripasso base di python
- 0.2.python-basic_example_of_performance_testing.ipynb: esempio di misurazione di diversi approcci per lo svolgimento di uno stesso task, differenze di prestazioni tra un **approccio pythonico** e un approccio scolastico
- 1.0.python-base.ipynb: approfondimento su condizioni, loop ed iteratori. In particolare come creare il proprio iterabile attraverso l'interazione con i **magic method**
- 2.0.python-function_decorators.ipynb: breve approfondimento su **class factory** e **decorators** (WIP)


### branch: python-intermidiate

